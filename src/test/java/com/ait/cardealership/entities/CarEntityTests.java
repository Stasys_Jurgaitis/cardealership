package com.ait.cardealership.entities;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import static org.junit.jupiter.api.Assertions.*;

import com.ait.cardealership.DAO.DAO;
import com.ait.cardealership.entities.Car;

class CarEntityTests {
	
	@Test
	void testSettersGettersAndNoArgConstructor() {
		final Car car = new Car();
		car.setCarId(1);
		car.setMake("Opel");
		car.setModel("Corsa");
		car.setYear("2002");
		car.setMilage(80000);
		car.setRegistration("02LD5555");
		car.setPrice(2000);

		assertEquals(car.getCarId(), 1);
		assertEquals(car.getMake(), "Opel");
		assertEquals(car.getModel(), "Corsa");
		assertEquals(car.getYear(), "2002");
		assertEquals(car.getMilage(), 80000);
		assertEquals(car.getRegistration(), "02LD5555");
		assertEquals(car.getPrice(), 2000);
	}

	@Test
	void testFullArgsConstructor() {
		Car car = new Car(2, "Suzuki", "Jimmy", "1998", 125000, "02WW54321", 1500);
		
		assertEquals(car.getCarId(), 2);
		assertEquals(car.getMake(), "Suzuki");
		assertEquals(car.getModel(), "Jimmy");
		assertEquals(car.getYear(), "1998");
		assertEquals(car.getMilage(), 125000);
		assertEquals(car.getRegistration(), "02WW54321");
		assertEquals(car.getPrice(), 1500);
	}
	
}

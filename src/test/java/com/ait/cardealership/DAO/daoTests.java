package com.ait.cardealership.DAO;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import static org.junit.jupiter.api.Assertions.*;

import com.ait.cardealership.entities.Car;

class Tests {

	DAO dao;
	Car car4test = new Car();

	@BeforeEach
	void setUp() throws Exception {
		dao = new DAO();
		car4test.setMake("Ford");
		car4test.setModel("Focus");
		car4test.setYear("2006");
		car4test.setMilage(50000);
		car4test.setRegistration("06LD1452");
		car4test.setPrice(3500);
		
	}

	@AfterEach
	void tearDown() throws Exception {
		dao = null;
		car4test = null;
	}

	@Test
	void testAddGetDeleteCar() {
		Car car4testFromDB = new Car();
		dao.addNewCar(car4test);
		for (final Car car : dao.getAllCars()) {
			if (car.getRegistration().equals(car4test.getRegistration())) {
				car4testFromDB = car;
			}
		}
		
		assertEquals(car4test.getMake(), car4testFromDB.getMake());
		assertEquals(car4test.getModel(), car4testFromDB.getModel());
		assertEquals(car4test.getYear(), car4testFromDB.getYear());
		assertEquals(car4test.getMilage(), car4testFromDB.getMilage());
		assertEquals(car4test.getRegistration(), car4testFromDB.getRegistration());
		assertEquals(car4test.getPrice(), car4testFromDB.getPrice());

		dao.deleteCar(car4testFromDB.getRegistration());
	}
	
	@Test
	void testEditCar() {

		final Car carFromDBBackup = dao.getAllCars().get(0);
		car4test.setRegistration("04D1234");
		dao.editCar(car4test);

		final Car carFromDB = dao.getAllCars().get(0);

		assertEquals(car4test.getMake(), carFromDB.getMake());
		assertEquals(car4test.getModel(), carFromDB.getModel());
		assertEquals(car4test.getYear(), carFromDB.getYear());
		assertEquals(car4test.getMilage(), carFromDB.getMilage());
		assertEquals(car4test.getRegistration(), carFromDB.getRegistration());
		assertEquals(car4test.getPrice(), carFromDB.getPrice());
		dao.editCar(carFromDBBackup);
	}

}

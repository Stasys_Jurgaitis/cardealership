package com.ait.cardealership.menu;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import static org.junit.jupiter.api.Assertions.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.jupiter.api.Test;

public class menuTests {

	Menu menu;
	private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
	private final PrintStream originalOut = System.out;

	@BeforeEach
	void setUp() throws Exception {
		System.setOut(new PrintStream(outContent));
		menu = new Menu();
	}

	@AfterEach
	void tearDown() throws Exception {
		System.setOut(originalOut);
		menu = null;
	}

	@Test
	void testGetAllcars() {
		menu.showCars();
		final String first2Records4test = "Car ID = 1, Make = Opel, Model = Astra, Year = 2004, Mileage = 50000, Registration = 04D1234, Price = 1200\r\n"
				+ "Car ID = 2, Make = Peugeot, Model = 206, Year = 1999, Mileage = 150000, Registration = 99D999, Price = 600";
		final String allCars = outContent.toString();
		final String first2Records = allCars.substring(0, 214);

		assertEquals(first2Records4test, first2Records.trim());
	}

}

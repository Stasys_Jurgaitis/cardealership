package com.ait.cardealership.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.ait.cardealership.entities.Car;

public class DAO {

	public void addNewCar(final Car car) {
		Connection conn = null;
		PreparedStatement prepStm = null;
		try {
			conn = ConnectionHelper.getConnection();
			prepStm = (PreparedStatement) conn.prepareStatement("INSERT INTO vehicles VALUES (null, '" + car.getMake()
					+ "','" + car.getModel() + "','" + car.getYear() + "', " + car.getMilage() + ",'"
					+ car.getRegistration() + "'," + car.getPrice() + ");");
			prepStm.executeUpdate();
			System.out.println("Car record added");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ConnectionHelper.close(conn);
		}
	}

	public List<Car> getAllCars() {
		final List<Car> list = new ArrayList<Car>();
		Connection conn = null;
		final String sql = "SELECT * FROM vehicles";
		try {
			conn = ConnectionHelper.getConnection();
			final Statement stm = conn.createStatement();
			final ResultSet rset = stm.executeQuery(sql);
			while (rset.next()) {
				final Car car = new Car();
				car.setCarId(rset.getInt("id"));
				car.setMake(rset.getString("make"));
				car.setModel(rset.getString("model"));
				car.setYear(rset.getString("year"));
				car.setMilage(rset.getInt("mileage"));
				car.setRegistration(rset.getString("registration"));
				car.setPrice(rset.getInt("price"));
				list.add(car);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ConnectionHelper.close(conn);
		}
		return list;
	}

	public void editCar(final Car car) {
		Connection conn = null;
		PreparedStatement prepstm = null;
		try {
			conn = ConnectionHelper.getConnection();
			prepstm = (PreparedStatement) conn.prepareStatement("UPDATE vehicles SET make = '" + car.getMake()
					+ "', model = '" + car.getModel() + "', year = '" + car.getYear() + "', mileage = "
					+ car.getMilage() + ", registration = '" + car.getRegistration() + "', price = " + car.getPrice()
					+ " where registration = '" + car.getRegistration() + "';");
			prepstm.executeUpdate();
			System.out.println("Car record updated");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ConnectionHelper.close(conn);
		}

	}

	public void deleteCar(final String registration) {
		Connection conn = null;
		PreparedStatement prepstm = null;
		try {
			conn = ConnectionHelper.getConnection();
			prepstm = (PreparedStatement) conn
					.prepareStatement("DELETE FROM vehicles WHERE registration = '" + registration + "';");
			prepstm.executeUpdate();
			System.out.println("Car deleted");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ConnectionHelper.close(conn);
		}

	}

}

package com.ait.cardealership.inputScanner;

import java.util.Scanner;

public class InputScannerHelper {
	final static public Scanner scanner = new Scanner(System.in);

	public static String getString() {
		return scanner.nextLine();
	}

	public static int getNumber() {
		int input = 0;
		try {
			input = Integer.parseInt(scanner.nextLine());
		} catch (Exception e) {
			System.out.println("Must be valid number");
			input = getNumber();
		}
		return input;
	}

	public static String getMake() {
		String make = "";
		make = scanner.nextLine();
		while ("".equals(make)) {
			System.out.println("Make cannot be empty");
			make = scanner.nextLine();
		}
		return make;
	}

	public static String getModel() {
		String model = "";
		model = scanner.nextLine();
		while ("".equals(model)) {
			System.out.println("Model cannot be empty");
			model = scanner.nextLine();
		}
		return model;
	}
	
	public static String getYear() {
		String year = "";
		year = scanner.nextLine();
		while ("".equals(year)) {
			System.out.println("year cannot be empty");
			year = scanner.nextLine();

		}
		return year;
	}

	public static int getMileage() {
		int mileage = 0;
		try {
			mileage = Integer.parseInt(scanner.nextLine());
		} catch (Exception e) {
			System.out.println("Mileage must be valid number");
			mileage = getMileage();
		}
		return mileage;
	}
	
	public static String getRegistration() {
		String registration = "";
		registration = scanner.nextLine();
		while ("".equals(registration)) {
			System.out.println("Registration cannot be empty");
			registration = scanner.nextLine();

		}
		return registration;
	}
	
	public static int getPrice() {
		int price = 0;
		try {
			price = Integer.parseInt(scanner.nextLine());
		} catch (Exception e) {
			System.out.println("Price must be valid number");
			price = getPrice();
		}
		return price;
	}
}

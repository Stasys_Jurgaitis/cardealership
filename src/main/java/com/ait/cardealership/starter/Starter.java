package com.ait.cardealership.starter;

import com.ait.cardealership.menu.Menu;

public class Starter {
	Menu menu = new Menu();

	public final static void main(final String[] args) {
		new Starter().start();
	}

	public void start() {
		menu.startMenu();
	}

}

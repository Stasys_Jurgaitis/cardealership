package com.ait.cardealership.menu;

import com.ait.cardealership.entities.Car;

import java.util.List;

import com.ait.cardealership.DAO.DAO;
import com.ait.cardealership.inputScanner.InputScannerHelper;

public class Menu {
	DAO dao = new DAO();

	public void startMenu() {
		System.out.println("Welcome to Car Management program");

		String choice = "0";
		while (true) {
			choice = userChoice();
			if ("1".equals(choice)) {
				addNewCar();
			} else if ("2".equals(choice)) {
				showCars();
			} else if ("3".equals(choice)) {
				editCar();
			} else if ("4".equals(choice)) {
				deleteCar();
			} else if ("5".equals(choice)) {
				choice = "5";
				System.out.println("Exiting program, have a great day!");
				System.exit(1);
			} else {
				System.out.println("Invalid input");
			}
		}
	}

	public String userChoice() {
		System.out.println("What would you like to do?");
		System.out.println("1. Add new car ");
		System.out.println("2. Show cars");
		System.out.println("3. Edit car details");
		System.out.println("4. Delete car");
		System.out.println("5. Exit");
		return InputScannerHelper.getString();
	}

	public void addNewCar() {
		final Car car = new Car();
		System.out.println("Please enter make");
		car.setMake(InputScannerHelper.getMake());
		System.out.println("Please enter model");
		car.setModel(InputScannerHelper.getModel());
		System.out.println("Please enter year");
		car.setYear(InputScannerHelper.getYear());
		System.out.println("Please enter milage");
		car.setMilage(InputScannerHelper.getMileage());
		System.out.println("Please enter registration");
		car.setRegistration(InputScannerHelper.getRegistration());
		System.out.println("Please enter price");
		car.setPrice(InputScannerHelper.getPrice());
		dao.addNewCar(car);
	}

	public void showCars() {
		final List<Car> allCars = dao.getAllCars();
		if (!allCars.isEmpty()) {
			for (final Car car : allCars) {
				System.out.println("Car ID = " + car.getCarId() + ", Make = " + car.getMake() + ", Model = "
						+ car.getModel() + ", Year = " + car.getYear() + ", Mileage = " + car.getMilage()
						+ ", Registration = " + car.getRegistration() + ", Price = " + car.getPrice());
			}
		} else {
			System.out.println("Vehicle table is empty");
		}

	}
	
	public void editCar() {
		final Car carToEdit = new Car();
		System.out.println("Please enter car registration");
		final String registration = InputScannerHelper.getRegistration();
		boolean registrationExists = false;

		for (final Car carFromDB : dao.getAllCars()) {
			if (registration.equals(carFromDB.getRegistration())) {
				registrationExists = true;
				carToEdit.setRegistration(registration);
			}
		}
		if (!registrationExists) {
			System.out.println("No Car with such registration found");
		} else {
			System.out.println("Please enter new make");
			carToEdit.setMake(InputScannerHelper.getMake());
			System.out.println("Please enter new model");
			carToEdit.setModel(InputScannerHelper.getModel());
			System.out.println("Please enter new year");
			carToEdit.setYear(InputScannerHelper.getYear());
			System.out.println("Please enter new mileage");
			carToEdit.setMilage(InputScannerHelper.getMileage());
			System.out.println("Please enter new price");
			carToEdit.setPrice(InputScannerHelper.getPrice());
			dao.editCar(carToEdit);
		}
	}
	
	public void deleteCar() {
		System.out.println("Please enter car registration");
		final String registration = InputScannerHelper.getRegistration();
		boolean registrationExists = false;

		for (final Car car : dao.getAllCars()) {
			if (registration.equals(car.getRegistration())) {
				registrationExists = true;
				dao.deleteCar(registration);
			}
		}
		if (!registrationExists) {
			System.out.println("No Car with such registration found");
		}
	}

}

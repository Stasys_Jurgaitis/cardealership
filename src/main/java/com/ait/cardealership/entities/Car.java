package com.ait.cardealership.entities;

public class Car {
	private int carId;
	private String make;
	private String model;
	private String year;
	private int milage;
	private String registration;
	private int price;

	public Car() {

	}

	public Car(final int carId, final String make, final String model, final String year, final int milage, final String registration, final int price) {
		super();
		this.carId = carId;
		this.make = make;
		this.model = model;
		this.year = year;
		this.milage = milage;
		this.registration = registration;
		this.price = price;
	}

	public int getCarId() {
		return carId;
	}

	public void setCarId(final int carId) {
		this.carId = carId;
	}

	public String getMake() {
		return make;
	}

	public void setMake(final String make) {
		this.make = make;
	}

	public String getModel() {
		return model;
	}

	public void setModel(final String model) {
		this.model = model;
	}

	public String getYear() {
		return year;
	}

	public void setYear(final String year) {
		this.year = year;
	}

	public int getMilage() {
		return milage;
	}

	public void setMilage(final int milage) {
		this.milage = milage;
	}

	public String getRegistration() {
		return registration;
	}

	public void setRegistration(final String registration) {
		this.registration = registration;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(final int price) {
		this.price = price;
	}

}
